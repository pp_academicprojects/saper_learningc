#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

int amountToWin;
int alreadyShowed;
int mapSize = 8;
int bombsOnMap = 10;
char** map;
char** bombs;

int GenerateRandomNumber(){
	return (rand() % 8) + 1 + '0';
}

char GenerateRandomLetter(){
	return 'A' + (random() % 8);
}

int LetterToNumber(char letter){
	char dummyLetter = letter - 'A';
	int retLetter = dummyLetter + '0';
	return retLetter + 1;
}

void PrintMap(){
	printf("\n");
	for (int i=0; i <= mapSize; i++){
		printf("%s \n", map[i]);
	}
}

void PrintBombs(){
	printf("\nBomby:.\n");
	for (int i=0; i < bombsOnMap; i++){
		printf("%s \n", bombs[i]);
	}
}

void CreateMap(){
	map = malloc((mapSize + 1) * sizeof(char*));
	if (map == NULL){
		perror("Unable to alloce space for map");
		exit(1);
	}
	
	for (int i=0; i<=mapSize; i++){
		char *arg = malloc(sizeof(char*));
		if(i == 0){
			arg = "0ABCDEFGH";
		}else{
			arg[0] = i + '0';
			for (int j=1; j <= mapSize; j++){
				arg[j] = 'O';
			}
		}
		map[i] = arg;
	}
}

void GenerateBombs(){
	
	bombs = malloc(bombsOnMap * sizeof(char*));
	
	for (int i =0; i< bombsOnMap; i++){
		duplication:
		while (1){
			char* generatedLocation = malloc(sizeof(char*));
			generatedLocation[0] = GenerateRandomLetter();
			generatedLocation[1] = GenerateRandomNumber();
			for (int j = 0; j < i; j++){
				if (bombs[j][0]==generatedLocation[0] && bombs[j][1]==generatedLocation[1]){
					goto duplication;
				}
			}
			bombs[i] = generatedLocation;
			break;
		}
	}	
}

char *getInput(char *buffer, size_t bufsize){
	//&buffer - adress if first character where position where input string will be stored
	//&bufsize - adress of the size of buffer
	int i = getline(&buffer,&bufsize,stdin);
	if (i == -1){
		perror("Exception. Can't load command content.\n");
		exit(-1);
	}
	buffer[strlen(buffer)-1] = 0;
	return buffer;
}

bool shootedIntoBomb(char* input){
	for (int i = 0; i<bombsOnMap; i++){
		if (bombs[i][0] == input[0] && bombs[i][1] == input[1]){
			return true;
		}
	}
	return false;
}
bool isThereBomb(int row, int column){
	for (int i = 0; i<bombsOnMap; i++){
		if (LetterToNumber(bombs[i][0]) - '0' == column && bombs[i][1] - '0' == row){
			return true;
		}
	}
	return false;
}

bool shootedIntoMarkedSpot(char* input){
	int row = input[1] - '0';
	int column = LetterToNumber(input[0]) - '0';
	return map[row][column] != 'O';
}

int amountOfBombsNearSpot(int row, int column){
	bool checkLeft = column > 1;
	bool checkRight = column < 8;
	bool checkUp = row > 1;
	bool checkDown = row < 8;
	
	int ret = 0;
	
	if (checkLeft && checkUp){
		if (isThereBomb(row - 1, column -1)){
			ret = ret + 1;
		}
	}	
	if (checkUp){
		if (isThereBomb(row - 1, column)){
			ret = ret + 1;
		}
	}	
	if (checkRight && checkUp){
		if (isThereBomb(row - 1, column + 1)){
			ret = ret + 1;
		}
	}	
	if (checkRight){
		if (isThereBomb(row, column + 1)){
			ret = ret + 1;
		}
	}	
	if (checkRight && checkDown){
		if (isThereBomb(row + 1, column + 1)){
			ret = ret + 1;
		}
	}	
	if (checkDown){
		if (isThereBomb(row + 1, column)){
			ret = ret + 1;
		}
	}	
	if (checkLeft && checkDown){
		if (isThereBomb(row + 1, column -1)){
			ret = ret + 1;
		}
	}	
	if (checkLeft){
		if (isThereBomb(row, column -1)){
			ret = ret + 1;
		}
	}
	
	return ret;
}

void markGoodGuess(char* input){
	int row = input[1] - '0';
	int column = LetterToNumber(input[0]) - '0';
	int nearBombs = amountOfBombsNearSpot(row, column);
	map[row][column] = nearBombs + '0';
}

void markBombs(){
	for (int i=0; i<bombsOnMap; i++){
		int row = bombs[i][1] - '0';
		int column = LetterToNumber(bombs[i][0]) - '0';
		map[row][column] = 'X';
	}
}

int main(int argc, char* argv[]){
	
	alreadyShowed = 0;
	amountToWin = mapSize * mapSize - bombsOnMap;
	
	//place to store string input
    char *buffer;
	//size_t - special type of int required by getline function
    size_t bufsize = 32;
	
	//assign 32 bites of memory to buffer location
    buffer = (char *)malloc(bufsize * sizeof(char));
	//check if if was succesful (was free space to locate)
    if( buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }
	
	CreateMap();
	GenerateBombs();
	PrintMap();	
	printf("\nPodaj wspĂłĹrzÄdne pola do sprawdzenia.\n");
	if (argc == 2){
		PrintBombs();
	}
	
	while(1){
		char *p = getInput(buffer, bufsize);
		if (strlen(p) < 2){
			printf("Proszę o podanie współrzędnych w formacie XY,\ngdzie X to litera z zakresu a-h,\na Y to cyfra z zakresu 1-8\n");
			continue;
		}
		if (!((p[0] >= 'A' && p[0] <= 'H') )){//|| (p[0] >= 'a' && p[0] <= 'h'))){
			printf("Pierwsza współrzędna musi być literą z zakresu A-H.\n");//a-h lub A-H.\n");
			continue;
		}
		if (!(p[1] >= '1' && p[1] <= '8')){
			printf("Druga współrzędna musi być cyfrą z zakresu 1-8.\n");
			continue;
		}
		if (shootedIntoBomb(p)){
			markBombs();
			PrintMap();
			printf("\nNiestety w podanym polu jest bomba, gra zakończona.\n");
			break;
		}else if(shootedIntoMarkedSpot(p)){
			printf("Wybrane pole zostało już oznaczone jako bezpieczne, wybierz inne.\n");
			continue;
		}else{
			printf("\nPole bezpieczne.\n");
			alreadyShowed = alreadyShowed +1;
			markGoodGuess(p);
			PrintMap();
			printf("\nPodaj współrzędne pola do sprawdzenia.\n");
		}
		if (alreadyShowed == amountToWin){
			printf("Gratuluję, udało Ci się odgadnąć położenie wszystkich min. :)");
		}
		//printf("%d \n", GenerateRandomNumber());
		//printf("%c \n", GenerateRandomLetter());
		
	}
	
    return(0);
}


























	